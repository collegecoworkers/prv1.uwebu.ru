<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $dataProvider backend\modules\contact\models\Contact */
/* @var $searchModel backend\modules\contact\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Кандидаты');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
		'pageSize' => 20,
	],
]);
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
			<div class="contact-index">
				<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/side/create') ?>
				<div class="fa-br"></div>
				<br>
				<?php
					echo GridView::widget([
						'dataProvider' => $dataProvider,
						'layout' => "{items}\n{pager}",
						'columns' => [
							// ['class' => 'yii\grid\SerialColumn'],
							'id',
							[
								'label' => 'Ник',
								'attribute' => 'title',
								'format' => 'raw',
								'value' => function($dataProvider){
									return $dataProvider->title;
								},
							],
							[
								'label' => 'Ник',
								'attribute' => 'desc',
								'format' => 'raw',
								'value' => function($dataProvider){
									return $dataProvider->desc;
								},
							],
							[
								'class' => 'yii\grid\ActionColumn',
								'header'=>'Действия', 
								'headerOptions' => ['width' => '80'],
								'template' => '{update} {delete}{link}',
							],
						],
					]);
				?>
			</div>
		</div>
	</div>
</div>
