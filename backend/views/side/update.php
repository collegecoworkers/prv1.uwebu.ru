<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Кандидат');

?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $model->title ?></div>
        <div class="panel-body">

<?php

$this->title = Yii::t('app', 'Отредактировать');

$this->params['breadcrumbs'][] = ['label' => 'Кандидаты'];
$this->params['breadcrumbs'][] = ['label' => $model->title];
?>
<div class="link-update">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['placeholder' => 'Ник']) ?>
	<?= $form->field($model, 'desc')->textArea(['placeholder' => 'Ник']) ?>

	<div class="form-group center">
		<?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' =>  'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>


</div>

        </div>
    </div>
</div>
