<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

use common\models\Choice;
use common\models\Side;

$this->title = Yii::t('app', 'Выборы');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

<div class="contact-index">
		<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/site/create') ?>
	<div class="fa-br"></div>
	<br>
	<?php
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'label' => 'Название',
				'attribute' => 'title',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->title;
				},
			],
			[
				'label' => 'Описание',
				'attribute' => 'desc',
				'format' => 'raw',
				'value' => function($dataProvider){
					return $dataProvider->desc;
				},
			],
			[
				'label' => 'Левый кандидат',
				'attribute' => 'left_side',
				'format' => 'raw',
				'value' => function($dataProvider){
					$side = Side::find()->where(['id' => $dataProvider->left_side])->one();
					$choice = Choice::find()->where(['election_id' => $dataProvider->id, 'side_id' => $dataProvider->left_side])->count();
					return Html::a($side['title'], ['side/index/'.$side['id']]) . '('. $choice .')';
				},
			],
			[
				'label' => 'Правый кандидат',
				'attribute' => 'right_side',
				'format' => 'raw',
				'value' => function($dataProvider){
					$side = Side::find()->where(['id' => $dataProvider->right_side])->one();
					$choice = Choice::find()->where(['election_id' => $dataProvider->id, 'side_id' => $dataProvider->right_side])->count();
					return Html::a($side['title'], ['side/index/'.$side['id']]) . '('. $choice .')';
				},
			],
			[
				'label' => 'Статус',
				'attribute' => 'status',
				'format' => 'raw',
				'value' => function($dataProvider){
					switch ($dataProvider->status) {
						case '1': return 'Активен';
						default: return 'Завершен';
					}
				},
			],
			[
				'label' => 'Победитель',
				'attribute' => 'result',
				'format' => 'raw',
				'value' => function($dataProvider){
					$side = Side::find()->where(['id' => $dataProvider->result])->one();
					if($side)
						return Html::a($side['title'], ['side/index/'.$side['id']]);
					else
						return 'Не определен';
				},
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{update} {delete}{link}',
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
