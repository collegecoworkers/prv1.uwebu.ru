<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Новые выборы');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $model->title ?></div>
        <div class="panel-body">

<?php


$this->title = Yii::t('app', 'Создание выборов');

?>
<div class="link-update">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['placeholder' => '']) ?>
	<?= $form->field($model, 'desc')->textArea(['placeholder' => '']) ?>
	<?= $form->field($model, 'left_side')->dropDownList($sides) ?>
	<?= $form->field($model, 'right_side')->dropDownList($sides) ?>
	<?= $form->field($model, 'status')->dropDownList([
		'1' => 'Активен',
		'0' => 'Завершен',
	]) ?>

	<div class="form-group center">
		<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' =>  'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>


</div>

        </div>
    </div>
</div>
