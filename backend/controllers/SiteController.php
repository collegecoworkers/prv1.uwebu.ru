<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\Elections;
use common\models\Side;
use common\models\Choice;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'signup',
							'login',
							'error'
						],
						'allow' => true,
					],
					[
						'actions' => [
							'logout',
							'index',
							'create',
							'update',
							'delete',
						],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		$model = Elections::find();
		return $this->render('index', ['model' => $model]);
	}

	public function actionCreate() {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Elections();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$model->title = Yii::$app->request->post()['Elections']['title'];
			$model->desc = Yii::$app->request->post()['Elections']['desc'];
			$model->left_side = Yii::$app->request->post()['Elections']['left_side'];
			$model->right_side = Yii::$app->request->post()['Elections']['right_side'];
			$model->status = Yii::$app->request->post()['Elections']['status'];
			
			$model->save();

			return $this->redirect(['index']);
		}

		$sides = [];
		$all_sides = Side::find()->all();
		for ($i=0; $i < count($all_sides); $i++) $sides[$all_sides[$i]->id] = $all_sides[$i]->title;

		return $this->render('create', [
			'model' => $model,
			'sides' => $sides,
		]);
	}

	public function actionUpdate($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Elections::findIdentity($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Elections']['title'];
			$model->desc = Yii::$app->request->post()['Elections']['desc'];
			
			$model->status = Yii::$app->request->post()['Elections']['status'];
			
			if($model->status == 0){
				$choices_left = Choice::find()->where(['election_id' => $model->id, 'side_id' => $model->left_side])->count();
				$choices_right = Choice::find()->where(['election_id' => $model->id, 'side_id' => $model->right_side])->count();
				if($choices_left > $choices_right){
					$model->result = $model->left_side;
				} elseif($choices_left < $choices_right){
					$model->result = $model->right_side;
				} else {
					$model->result = 0;
				}
			} else {
				$model->result = 0;
			}

			$model->save();
			return $this->redirect(['index']);
		}

		$sides = [];
		$all_sides = Side::find()->all();
		for ($i=0; $i < count($all_sides); $i++) $sides[$all_sides[$i]->id] = $all_sides[$i]->title;

		return $this->render('update', [
			'model' => $model,
			'sides' => $sides,
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Elections::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				if($user->isAdmin($user['id'])) {
					Yii::$app->user->login($user);
					return $this->goHome();
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}
}
