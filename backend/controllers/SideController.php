<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Side;

class SideController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['error'],
						'allow' => true,
					],
					[
						'actions' => [
							'index', 
							'single', 
							'update', 
							'delete', 
							'create'
						],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		$model = Side::find();
		return $this->render('index', [
			'model' => $model,
		]);
	}

	public function actionSingle($id) {
		$model = Side::find()->where(['id'=>$id]);
		return $this->render('index', [
			'model' => $model,
		]);
	}
	
	public function actionUpdate($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Side::findId($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('update', [
			'model' => $model
		]);
	}

	public function actionCreate() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Side();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {

			$model->title = Yii::$app->request->post()['Side']['title'];
			$model->desc = Yii::$app->request->post()['Side']['desc'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = Side::findId($id);
		$model->delete();
		return $this->redirect(['index']);
	}

}
