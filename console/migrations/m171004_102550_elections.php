<?php

use yii\db\Migration;

class m171004_102550_doktor extends Migration {

	public function safeUp() {
		$this->createTable('doktor', [
			'id' => $this->primaryKey(),
			'title' => $this->string(60),
			'desc' => $this->text(),
			'left_side' => $this->integer(11)->notNull(),
			'right_side' => $this->integer(11)->notNull(),
			'status' => $this->integer(1)->notNull(),
			'result' => $this->integer(11)->notNull(),
			'created_at' => $this->integer(11)->notNull(),
			'updated_at' => $this->integer(11)->notNull(),
		]);
	}

	public function safeDown() {
		echo "m171004_102550_doktor cannot be reverted.\n";

		return false;
	}

	/*
	// Use up()/down() to run migration code without a transaction.
	public function up() {

	}

	public function down() {
		echo "m171004_102550_doktor cannot be reverted.\n";

		return false;
	}
	*/
}
