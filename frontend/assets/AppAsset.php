<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'/plugins/bootstrap/css/bootstrap.min.css',
		'/plugins/font-awesome/css/font-awesome.css',
		'/plugins/elegant_font/css/style.css',
		'/css/styles.css',
	];
	public $js = [
		'/plugins/jquery-1.12.3.min.js',
		'/plugins/bootstrap/js/bootstrap.min.js',
		'/plugins/jquery-match-height/jquery.matchHeight-min.js',
		'/js/main.js',
	];
	public $depends = [
		// 'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
	];
}
