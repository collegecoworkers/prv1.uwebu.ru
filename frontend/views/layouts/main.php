<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<title><?= Html::encode($this->title) ?></title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">    
	<!-- <link rel="shortcut icon" href="favicon.ico">   -->
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<?= Html::csrfMetaTags() ?>
	<?php $this->head() ?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head> 
<body>
<?php $this->beginBody() ?>
<body class="landing-page">   

	<div class="page-wrapper">
		<!-- ******Header****** -->
		<header class="header">
			<div class="container">
				<div class="branding">
					<h1 class="logo">
						<a href="/">
							<!-- <span aria-hidden="true" class="icon_documents_alt icon"></span> -->
							<span class="text-highlight">Твой</span><span class="text-bold">выбор</span>
						</a>
					</h1>
				</div><!--//branding-->
				<nav class="nav nav-pills pull-right">
					<div id="navbar" class="collapse navbar-collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href="/">Главная</a></li>
							<?php if (Yii::$app->user->isGuest): ?>
								<li><a href="/site/signup">Регистрация</a></li>
								<li><a href="/site/login">Вход</a></li>
							<?php else: ?>
								<li><a><?= \Yii::$app->user->identity->username ?></a></li>
								<li><a href="/site/logout">Выход</a></li>
							<?php endif ?>
						</ul>
					</div>
				</nav><!--//nav-->
			</div><!--//container-->
		</header><!--//header-->
		
			<?= Alert::widget() ?>
			<?= $content ?>

	</div><!--//page-wrapper-->
	
	<footer class="footer text-center">
		<div class="container">
			<small class="copyright">This is my site. And that's for sure!</small>
		</div><!--//container-->
	</footer><!--//footer-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
