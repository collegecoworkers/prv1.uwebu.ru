<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use common\models\Choice;

/* @var $this yii\web\View */

$this->title = 'Сделай выбор';

Yii::$app->view->params['page_title'] = [
	explode(" vs ", $model['title'])[0] . ' v',
	's ' . explode(" vs ", $model['title'])[1]
	];

$colors = [
	'item-green',
	'item-pink',
	'item-blue',
	'item-purple',
	'item-primary',
	'item-orange',
];



function formVoice($model, $choice, $the_side) {
	$voice = Choice::find()->where(['election_id' => $model['id'], 'user_id' => Yii::$app->user->getId()])->one();
?>
	<div class="item col-md-6 col-sm-6 col-xs-6">
		<p>Голосов: <span class="bg-primary">&nbsp;<?= 
			Choice::find()
			->where(['election_id' => $model['id'], 'side_id' => $the_side['id'],])
								// ->all()
			->count()
		?>&nbsp;</span></p>
		<div class="item-inner">
			<h3 class="title"><?= $the_side['title'] ?></h3>
			<p class="intro"><?= $the_side['desc'] ?></p>
		</div><!--//item-inner-->
		<hr>

		<?php if($voice): ?>
			<?php if($voice['side_id'] == $the_side['id']): ?>
				<p>Ваш выбор</p>
			<?php endif ?>
		<?php else: ?>
			<?php $form = ActiveForm::begin(); ?>
			<?= $form->field($choice, 'election_id')->hiddenInput(['value' => $model['id']])->label(false); ?>
			<?= $form->field($choice, 'side_id')->hiddenInput(['value' => $the_side['id']])->label(false); ?>
			<?= $form->field($choice, 'user_id')->hiddenInput(['value' => Yii::$app->user->getId()])->label(false); ?>
			<?= Html::submitButton('Голос', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
			<?php ActiveForm::end(); ?>
		<?php endif ?>
	</div><!--//item-->
<?php
}

?>

		<section class="cards-section text-center">
			<div class="container">
				<h2 class="title">Сделай свой выбор</h2>
				<div class="alert-danger">
					<?= $error ?>
				</div>
				<div id="cards-wrapper" class="cards-wrapper row">
					<?php formVoice($model, $choice, $left_side); ?>
					<?php formVoice($model, $choice, $right_side); ?>
				</div><!--//cards-->
				
			</div><!--//container-->
		</section><!--//cards-section-->