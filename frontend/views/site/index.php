<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */

$this->title = 'Сделай выбор';
Yii::$app->view->params['page_title'] = ['Твой', 'выбор'];

$colors = [
	'item-green',
	'item-pink',
	'item-blue',
	'item-purple',
	'item-primary',
	'item-orange',
];

?>

		<section class="cards-section text-center">
			<div class="container">
				<h2 class="title">Список доступных выборов</h2>
				<div class="intro">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae inventore, ad nam debitis voluptas beatae iusto dignissimos asperiores tempora perspiciatis molestias commodi natus rerum omnis ipsa magnam voluptates? Praesentium, dolore?</p>
				</div><!--//intro-->
				<div id="cards-wrapper" class="cards-wrapper row">
					<?php 
					foreach ($model as $elec):
					?>
					<div class="item <?= $colors[random_int(0, count($colors))] ?> col-md-6 col-sm-6 col-xs-6">
						<div class="item-inner">
							<h3 class="title"><?= $elec['title'] ?></h3>
							<p class="intro"><?= $elec['desc'] ?></p>
							<a class="link" href="/election/<?= $elec['id'] ?>"><span></span></a>
						</div><!--//item-inner-->
					</div><!--//item-->
					<?php endforeach ?>

				</div><!--//cards-->
				
			</div><!--//container-->
		</section><!--//cards-section-->