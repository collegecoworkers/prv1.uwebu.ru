<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\Elections;
use common\models\Side;
use common\models\Choice;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'signup'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			// 'verbs' => [
			// 	'class' => VerbFilter::className(),
			// 	'actions' => [
			// 		'logout' => ['post'],
			// 	],
			// ],
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			]
		];
	}

	public function actionIndex() {
		$model = Elections::find()->where(['status' => 1])->all();

		return $this->render('index', [
			'model' => $model,
		]);
	}

	public function actionElection($id) {

		$choice = new Choice();
		$error = null;

		if ($choice->load(Yii::$app->request->post())) {

			if (Yii::$app->user->isGuest) return $this->redirect(['site/login']);

			$choice->election_id = Yii::$app->request->post('Choice')['election_id'];
			$choice->side_id = Yii::$app->request->post('Choice')['side_id'];
			$choice->user_id = Yii::$app->request->post('Choice')['user_id'];

			if(Choice::validation($choice->election_id)){
				$choice->save();
			} else {
				$error = 'Вы уже проголосовали.';
			}
		}

		$model = Elections::find()->where(['id' => $id, 'status' => 1])->one();
		
		$left_side = Side::find()->where(['id' => $model['left_side']])->one();;
		$right_side = Side::find()->where(['id' => $model['right_side']])->one();;

		return $this->render('election', [
			'choice' => $choice,
			'error' => $error,
			'model' => $model,
			'left_side' => $left_side,
			'right_side' => $right_side,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			
			$model->attributes = Yii::$app->request->post('Login');

			if($model->validate()) {
			
				Yii::$app->user->login($model->getUser());

				return $this->goHome();
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function actionSignup() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['site/login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

}
