<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Elections extends ActiveRecord {

	public static function tableName() {
		return '{{%elections}}';
	}

	public function rules() {
		return [
			[['title'], 'string'],
			[['desc'], 'string'],
			[['left_side'], 'integer'],
			[['right_side'], 'integer'],
			[['status'], 'integer'],
			[['result'], 'integer'],
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

}
