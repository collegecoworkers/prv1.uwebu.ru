<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Choice extends ActiveRecord {

	public static function tableName() {
		return '{{%choice}}';
	}

	public static function findId($id) {
		return static::findOne(['id' => $id]);
	}

	public static function validation($election_id) {
		return !static::findOne(['election_id' => $election_id, 'user_id' => Yii::$app->user->getId()]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

}
