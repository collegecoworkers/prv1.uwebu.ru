<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Side extends ActiveRecord {

	public static function tableName() {
		return '{{%side}}';
	}

	public function rules() {
		return [
			['title', 'string'],
			['desc', 'string'],
		];
	}
	
	public static function findId($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

}
